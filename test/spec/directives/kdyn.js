'use strict';

describe('Directive: kdyn', function () {

  // load the directive's module
  beforeEach(module('kdynApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<kdyn></kdyn>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the kdyn directive');
  }));
});
