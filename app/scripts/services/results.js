'use strict';

/**
 * @ngdoc service
 * @name kdynApp.results
 * @description
 * # results
 * Service in the kdynApp.
 */
angular.module('kdynApp')
  .service('results', function () {
    var service = {};
    service.data = {};

    service.get = function() {
      return service.data;
    };
    service.set = function(results) {
      service.data = results;
    };

    return service;
  });
