'use strict';

/**
 * @ngdoc overview
 * @name kdynApp
 * @description
 * # kdynApp
 *
 * Main module of the application.
 */
angular
  .module('kdynApp', [
    'ngCookies',
    'ngSanitize',
    'ngStorage',
    'ui.router',
    'jsonFormatter',
    'base64',
    'ui.bootstrap'
  ]).run(function ($localStorage) {
    if (!$localStorage.testDatabase) {
      $localStorage.testDatabase = {};

    }
    if (!$localStorage.results) {
      $localStorage.results = {
        total: 0,
        success: 0,
        failure: 0
      }
    }
  }).config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('main', {
        url: '',
        templateUrl: '/views/main.html'
      })
      .state('test', {
        url: '/test',
        templateUrl: '/views/test.html'
      })
      .state('database', {
        url: '/database',
        templateUrl: '/views/database.html'
      })
  });
