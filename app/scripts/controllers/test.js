  'use strict';

/**
 * @ngdoc function
 * @name kdynApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the kdynApp
 */
angular.module('kdynApp')
  .controller('TestCtrl', function ($scope, results, $localStorage) {
    $scope.storage = $localStorage;
    var testDatabase = $scope.storage.testDatabase;
    $scope.users = [];
    _.each(testDatabase, function(value, key) {
      $scope.users.push(key);
    });
    $scope.test = test;
    function test(test) {
      var testResults = [];
      _.each(testDatabase, function(userdata, username) {
        var currentUserTemplate = userdata.template;
        var sum = 0;
        _.each(test, function(value, key) {
          var template = currentUserTemplate[key];
          _.each(value, function(sample, key) {
            if (template[key]) {
              sum += Math.abs(template[key] - sample);
            }
            console.log(sample);
          })
        });
        testResults.push({
          username: username,
          score: sum
        });
      });
      var sortedResults = _.sortBy(testResults, 'score');
      console.log(sortedResults);
      if (confirm(sortedResults[0].username+'\nCzy to Twoja nazwa użytkownika?')) {
        $localStorage.results.total++;
        $localStorage.results.success++;
      } else {
        $localStorage.results.total++;
        $localStorage.results.failure++;
      }
    }
  });
