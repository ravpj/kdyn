'use strict';

/**
 * @ngdoc function
 * @name kdynApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the kdynApp
 */
angular.module('kdynApp')
  .controller('MainCtrl', function ($scope, results, $localStorage) {
    $scope.storage = $localStorage;
    $scope.save = function (test) {
      $scope.storage.testDatabase[$scope.username] = {
        template: test
      };
    }
  });