'use strict';

/**
 * @ngdoc function
 * @name kdynApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the kdynApp
 */
angular.module('kdynApp')
  .controller('DbCtrl', function ($scope, results, $localStorage, $base64, $uibModal) {
    $scope.storage = $localStorage;
    $scope.clear = function() {
      $localStorage.testDatabase = {};
    };
});


