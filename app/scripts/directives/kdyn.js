'use strict';

/**
 * @ngdoc directive
 * @name kdynApp.directive:kdyn
 * @description
 * # kdyn
 */
angular.module('kdynApp')
  .directive('dynamics', function (results) {
    return {
      restrict: 'E',
      template: '' +
      '<div class="form-group"><button class="btn btn-warning" ng-show="started" ng-click="stop()">Zakończ test</button><button class="btn btn-primary" ng-hide="started" ng-click="start()">Rozpocznij test</button></div>' +
      '<div ng-show="started" class="form-group"><textarea class="form-control"></textarea></div>',
      scope: {
        test: '='
      },
      link: function postLink(scope, element, attrs) {
        var test = {
          DD: {},
          H: {},
          UD: {}
        };
        var input = element.find('textarea');

        var tempDD = null;
        var tempH = null;
        var tempUD = null;

        scope.start = startTest;
        scope.stop = stopTest;

        function startTest() {
          input.val('');
          scope.started = true;
          input.on('keydown', keyDownListener);
          input.on('keyup', keyUpListener);
          input.focus();
        }

        function stopTest() {
          scope.test = test;
          scope.started = false;
          input.off('keydown', keyDownListener);
          input.off('keyup', keyUpListener);
        }

function keyDownListener(event) {
  if (event.which !== 13) {
    if (tempDD !== null) {
      tempDD['kc2'] = event.which;
      tempDD['dt2'] = event.timeStamp;
      convertDD(tempDD);
      tempDD = null;
    } else {
      tempDD = {
        'kc1': event.which,
        'dt1': event.timeStamp
      }
    }
    if (tempH == null) {
      tempH = {
        'start': event.timeStamp,
        'kc': event.which
      }
    }
    if (tempUD) {
      tempUD['kc2'] = event.which;
      tempUD['dt2'] = event.timeStamp;
      convertUD(tempUD);
      tempUD = null;
    }

  }
}

function keyUpListener(event) {
  if (event.which !== 13) {
    if (tempH) {
      tempH.stop = event.timeStamp;
      convertH(tempH);
      tempH = null;
    }
    if (!tempUD) {
      tempUD = {
        'kc1': event.which,
        'dt1': event.timeStamp
      }
    }
  }
}

        function convertUD(udObj) {
          var keycode = udObj['kc1'] * 100 + udObj['kc2'];
          var time = udObj['dt2'] - udObj['dt1'];
          if (test.UD[keycode]) {
            test.UD[keycode] = (test.UD[keycode] + time) / 2;
          } else {
            test.UD[keycode] = time
          }
        }


        function convertH(hObj) {
          var keycode = hObj.kc;
          var time = hObj.stop - hObj.start;
          if (test.H[keycode]) {
            test.H[keycode] = (test.H[keycode] + time) / 2;
          } else {
            test.H[keycode] = time;
          }
        }

        function convertDD(ddObj) {
          var keycode = ddObj['kc1'] * 100 + ddObj['kc2'];
          var time = ddObj['dt2'] - ddObj['dt1'];
          if (test.DD[keycode]) {
            test.DD[keycode] = (test.DD[keycode] + time) / 2;
          } else {
            test.DD[keycode] = time
          }
        }
      }
    };
  });
